FROM node:10.7.0-alpine

WORKDIR /uddish
RUN rm -rf sessions/
RUN adduser uddish -s -u -D /bin/sh; mkdir -p sessions/; chown -R uddish:uddish /uddish;
RUN chmod -R 777 sessions/

COPY ./* sso-proxy/

RUN cd sso-proxy && npm install

ARG port=5000
ARG jwt_secret
ARG admin_egroup=ai-hadoop-admins
ARG user_egroup=ai-hadoop-users
ARG proxy_session_secret=Super-Secret-Session
ARG kapiato_url=https://hbackup-catalog.web.cern.ch/*
ARG oauth_client_id
ARG oauth_secret
ARG oauth_token_host=https://oauth.web.cern.ch
ARG oauth_token_path=/OAuth/Token
ARG oauth_authorize_path=/OAuth/Authorize
ARG oauth_redirect_uri
ARG oauth_scope=read
ARG oauth_resource_host=oauthresource.web.cern.ch
ARG oauth_resource_path_groups=/api/Groups
ARG oauth_resource_path_user=/api/User
ARG uddish_react_path_index=../hdbackup/index.html
ARG uddish_react_path=../hdbackup

EXPOSE ${port}
ENV PROXY_PORT=${port}
ENV SECRET_KEY=${jwt_secret}
ENV ADMIN_EGROUP=${admin_egroup}
ENV USER_EGROUP=${user_egroup}
ENV PROXY_SESSION_SECRET=${proxy_session_secret}
ENV KAPIATO_URL=${kapiato_url}
ENV OAUTH_CLIENT_ID=${oauth_client_id}
ENV OAUTH_SECRET=${oauth_secret}
ENV OAUTH_TOKEN_HOST=${oauth_token_host}
ENV OAUTH_TOKEN_PATH=${oauth_token_path}
ENV OAUTH_AUTHORIZE_PATH=${oauth_authorize_path}
ENV OAUTH_REDIRECT_URI=${oauth_redirect_uri}
ENV OAUTH_SCOPE=${oauth_scope}
ENV OAUTH_RESOURCE_HOST=${oauth_resource_host}
ENV OAUTH_RESOURCE_PATH_GROUPS=${oauth_resource_path_groups}
ENV OAUTH_RESOURCE_PATH_USER=${oauth_resource_path_user}
ENV UDDISH_REACT_PATH_INDEX=${uddish_react_path_index}
ENV UDDISH_REACT_PATH=${uddish_react_path}


USER uddish
#RUN ls -al
CMD [ "sh", "-c", "node sso-proxy/server.js" ]
