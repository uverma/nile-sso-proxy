/**
 * Module Imports
 */
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const auth = require('./auth');
const configs = require('./configs');
const api = require('./api');
const app = express();
var session = require('express-session');
var FileStore = require('session-file-store')(session);

/**
 * Express configuration
 */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  session({
    store: new FileStore(),
    resave: false,
    secret: process.env.PROXY_SESSION_SECRET || '',
    saveUninitialized: false,
    unset: 'destroy',
  }),
);

/**
 * Route Configuration
 */
// Requests for /api are proxied to add headers and tokens
// Do not forget to add the * on the API URL too. Check api.router
app.use('/api', api.router);

/**
 * DISCLAIMER: This had to be moved after express configuration
 * https://github.com/dvonlehman/express-request-proxy/issues/9#issuecomment-188862038
 */
app.use(bodyParser.json());

// Redirects user to CERN OAuth login page
app.get('/login', (req, res) => {
  console.debug('Being redirected to CERN OAuth service...');
  res.redirect(auth.authorizationURI);
});
// OAuth callback as defined by the RFC 6749
app.get('/callback', auth.oauthCallback);
// Redirects user to Nile Kafka Interface if logged in
app.get('/', (req, res) => {
  // User is not logged in. Redirects to /login
  if (!req.session.isAuthenticated) {
    console.debug('User is not authenticated. Redirecting to /login...');
    res.redirect('/login');
  }
  res.sendFile(path.join(__dirname, process.env.UDDISH_REACT_PATH_INDEX || ''));
});
// Destroy user's session
app.get('/logout', (req, res) => {
  console.debug(
    'Destroying session ' +
      req.session.id +
      'of user ' +
      req.session.user.username,
  );
  req.session.destroy(error => {
    if (error) console.debug(error);
  });
  res.send('Bye!');
});
// Send the session's user data to the angular app
app.get('/auth', (req, res) => {
  if (!req.session.isAuthenticated) res.redirect('/login');

  const response = {
    isAdmin: req.session.isAdmin,
    isAuthenticated: req.session.isAuthenticated,
    username: req.session.user.username,
    access_token: req.session.access_token,
  };
  res.send(response);
});

/**
 * Proxy Server Initialization
 */
app.set('port', configs.proxy.config.port);
app.use(
  express.static(path.join(__dirname, process.env.UDDISH_REACT_PATH || '')),
);
const server = http
  .createServer(app)
  .listen(configs.proxy.config.port, () =>
    console.log('Proxy running on port ', configs.proxy.config.port),
  );
