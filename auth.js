/**
 * Module Imports
 */
let simpleOAuth = require('simple-oauth2');
const configs = require('./configs');
const api = require('./api');

/**
 * OAuth2 Client
 */
const oauthClient = simpleOAuth.create({
  client: {
    id: configs.oauth.config.client_id,
    secret: configs.oauth.config.secret,
  },
  auth: {
    tokenHost: configs.oauth.config.tokenHost,
    tokenPath: configs.oauth.config.tokenPath,
    authorizePath: configs.oauth.config.authorizePath,
  },
});

/**
 * Redirects user to authorization page
 */
const authorizationURI = oauthClient.authorizationCode.authorizeURL({
  scope: configs.oauth.config.scope,
  state: configs.oauth.config.state,
});

/**
 * Callback for OAuth2.0 implementation
 * @param {Request} req
 * @param {Response} res
 */
async function oauthCallback(req, res) {
  const code = req.query.code;
  const options = {
    code: code,
  };

  try {
    // Get authorization code from CERN OAuth2 service
    const result = await oauthClient.authorizationCode.getToken(options);
    // Transform authroization code into an access token
    const token = oauthClient.accessToken.create(result);
    // Get user information and create session
    getUserInformationCreateSession(req.session, token, res);
  } catch (error) {
    console.error('Access Token Error', error.message);
    return res.status(500).json('Authentication Failed');
  }
}

/**
 * Wrapper method for readability purposes
 * @param {express-session} session
 * @param {AccessToken} token
 * @param {Response} res
 */
function getUserInformationCreateSession(session, token, res) {
  if (session === undefined) return res.status(500).json('Session is undefined');
  if (token === undefined) return res.status(500).json('Token is undefined');
  getUserInformation(session, token, res);
}

/**
 * Queries CERN OAuth2 service to get the user e-groups and user data
 * @param {Express-session} session
 * @param {AccessToken} token
 * @param {Response} res
 */
function getUserInformation(session, token, res) {
  let https = require('https');
  let options = {
    host: configs.oauth.config.resourceHost,
    path: configs.oauth.config.resourcePathGroups,
    headers: {
      Authorization: `Bearer ${token.token.access_token}`,
    },
  };
  // Querying user groups
  https
    .request(options, response => {
      let jsonGroups = '';
      response.on('data', function(data) {
        jsonGroups += data;
      });
      response.on('end', function() {
        const groups = JSON.parse(jsonGroups).groups;
        // After querying groups, lets query user data
        options.path = configs.oauth.config.resourcePathUser;
        https
          .request(options, response => {
            let jsonUser = '';
            response.on('data', function(data) {
              jsonUser += data;
            });
            response.on('end', function() {
              const user = JSON.parse(jsonUser);
              createSession(user, groups, session, res);
            });
          })
          .end();
      });
    })
    .end();
}

/**
 * Creates a express session based on the user, user's groups and token.
 * @param {user} user
 * @param {groups} groups
 * @param {express-session} session
 * @param {AccessToken} token
 * @param {Response} res
 */
function createSession(user, groups, session, res) {
  if (checkAdmin(groups)) session.isAdmin = true;

  const tokenJWT = api.genToken(user.username, groups);

  session.user = user;
  session.access_token = tokenJWT;
  session.isAuthenticated = true;
  res.redirect('/');
}

/**
 * Checks whether user belongs to the admin group or not
 * @param {groups} groups
 */
function checkAdmin(groups) {
  return groups.indexOf(configs.proxy.config.adminGroup) >= 0 ? true : false;
}

module.exports = {
  oauthClient: oauthClient,
  authorizationURI: authorizationURI,
  oauthCallback: oauthCallback,
};
