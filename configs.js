/**
 * Proxy Configuration
 */
const proxy = {
  config: {
    port: process.env.PROXY_PORT || '',
    secretKey: process.env.SECRET_KEY || '',
    adminGroup: process.env.ADMIN_EGROUP || '',
    userGroup: process.env.USER_EGROUP || '',
  },
};

/**
 * Api Configuration
 */
const api = {
  kapiato: {
    config: {
      url: process.env.KAPIATO_URL || '',
    },
  }
};

/**
 * OAuth2 Configuration
 */
const oauth = {
  config: {
    client_id: process.env.OAUTH_CLIENT_ID || '',
    secret: process.env.OAUTH_SECRET || '',
    tokenHost: process.env.OAUTH_TOKEN_HOST || '',
    tokenPath: process.env.OAUTH_TOKEN_PATH || '',
    authorizePath: process.env.OAUTH_AUTHORIZE_PATH || '',
    redirect_uri: process.env.OAUTH_REDIRECT_URI || '',
    scope: process.env.OAUTH_SCOPE || '',
    state: 'randomState',
    resourceHost: process.env.OAUTH_RESOURCE_HOST || '',
    resourcePathGroups: process.env.OAUTH_RESOURCE_PATH_GROUPS || '',
    resourcePathUser: process.env.OAUTH_RESOURCE_PATH_USER || '',
  },
}

module.exports = {
  proxy: proxy,
  api: api,
  oauth: oauth
};
