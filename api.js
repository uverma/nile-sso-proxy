/**
 * Module Imports
 */
const express = require('express');
const router = express.Router();
const configs = require('./configs');
let requestProxy = require('express-request-proxy');

const apiOpts = {
    // DISCLAIMER: do not forget to append the * in the end of the URL otherwise
    // the request proxy will not append the path and will always use root path ('/')
    url: configs.api.kapiato.config.url,
};

/**
 * Intercepts all the requests made to /api.
 * Adds groups header.
 */
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
function proxy(apiOpts) {
    const f = requestProxy(apiOpts);
    return function(req, res, next) {
        req.headers['Authorization'] = req.session.access_token;
        f(req, res, next);
    };
}

/**
 * Generate a JWT Token, containing username and groups, to be used in Kapiato
 * @param {string} username
 * @param {string} groups
 */
function generateToken(username, groups) {
    const jwt = require('jsonwebtoken');
    const payload = {
        username: username,
        groups: groups,
    };
    const token = jwt.sign(payload, configs.proxy.config.secretKey);
    return token;
}

/**
 * All methods being proxied
 */
router.get('/*', proxy(apiOpts));
router.post('/*', proxy(apiOpts));
router.put('/*', proxy(apiOpts));
router.delete('/*', proxy(apiOpts));
router.patch('/*', proxy(apiOpts));

module.exports = {
    router: router,
    genToken: generateToken,
};
